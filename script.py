import pandas as pd


def load_and_process_answers(file_path):
    with open(file_path, "r") as file:
        lines = file.readlines()

    answer_data = {}
    duplicate_ids = []

    for line in lines:
        id_part = line[:3]
        answers_part = line[3:].strip()

        if id_part in answer_data:
            duplicate_ids.append(id_part)
        else:
            answer_data[id_part] = answers_part

    if duplicate_ids:
        raise ValueError(f"Duplicate IDs found: {', '.join(duplicate_ids)}")

    return answer_data


def calculate_scores(answers):
    score = 0
    for i in range(0, len(answers), 4):
        sub_answers = answers[i:i + 4]
        correct_count = sub_answers.count('T')
        if correct_count == 4:
            score += 1
        elif correct_count == 3:
            score += 0.6
        elif correct_count == 2:
            score += 0.1
        else:
            score += 0

    return score


def load_participants_data(file_path):
    return pd.read_excel(file_path)


def main():
    answers_file_path = 'C:\\Users\\User\\Desktop\\ibo-results-calculator\\IBO-answer.txt'
    db_file_path = 'C:\\Users\\User\\Desktop\\ibo-results-calculator\\db.xlsx'

    answers = load_and_process_answers(answers_file_path)

    scores = {id: calculate_scores(answer) for id, answer in answers.items()}

    participants = load_participants_data(db_file_path)

    participants['Total Score'] = participants['ID'].astype(str).map(scores)

    print(participants)


if __name__ == "__main__":
    main()
